Paragraphs jQuery UI Tabs is a module to create paragraphs with tabs effect in your content.
It based on jQuery UI Tabs plugin which already included in core, so no need to install additional libraries.
See examples on https://jqueryui.com/tabs/

## Installation:

* Ensure Paragraphs module is enabled.
* Install this module as you normally would.
* Create new paragraphs bundle eg. 'tabs_paragraph'
* Add text fields for Title and Description to created bundle
* Add a new paragraphs multiple field to your content type or entity.
* Go to your entity's Manage display screen and choose Tabs Paragraph for the format
* Select proper fields for Title and Description
* You are now ready to add Tabs to your entity!

### Note

The source code has been adapted from [https://www.drupal.org/project/paragraphs_jquery_ui_accordion](https://www.drupal.org/project/paragraphs_jquery_ui_accordion)
