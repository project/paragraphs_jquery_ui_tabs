/**
 * @file
 * Paragraphs tabs script.
 */

(function ($) {
  Drupal.behaviors.paragraphs_jquery_ui_tabs = {
    attach: function (context, settings) {
      var tabs_id = Drupal.settings.paragraphs_jquery_ui_tabs.tabs_id;
      $(document).ready(function () {
        $('#' + tabs_id).tabs();
      });
    }
  };
}(jQuery));
